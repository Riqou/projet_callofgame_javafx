package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.util.Duration;



public class Controller {

    public int nbSteps;

    @FXML
    public TextArea textPassword;
    @FXML
    public Label Instructions;
    @FXML
    public Label Text;
    @FXML
    public ProgressBar LoadingBar;
    @FXML
    public Label LoadingLabel;
    @FXML
    public Button ValidButton;

    /**
     * Executer quand on appuie sur le bouton
     */
    public void validerButtonClicked() {
        String text = textPassword.getText();
        //this.affichage();
        if (this.nbSteps == 0 && text.equals("SERGEY")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();

        } else if (this.nbSteps == 1 && text.equals("Youki")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();

        } else if (this.nbSteps == 2 && text.equals("13/01")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();

        } else if (this.nbSteps == 3 && text.equals("-5")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 4 && text.equals("110")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 5 && text.equals("11")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 6 && text.equals("12")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 7 && text.equals("7")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 8 && text.equals("5")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 9 && text.equals("7989")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 10 && text.equals("-1A34CA2LL4OF21GA0ME")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        } else if (this.nbSteps == 11 && text.equals("")) {
            ValidButton.setDisable(true);
            Text.setText("Vérification");
            this.chargement();
        }
        else {
            Text.setText("Mécréant jamais vous n'obtiendrez ma formule !! ");
            //Problème, pas d'erreur mais plus de message non plus avec cette commande
            //Text.setTextFill(Color.web("#101010"));
        }

    }


    private void chargement() {
        for (int i = 0; i <= 100; i += 1) {
            int finalI = i;
            Timeline timer = new Timeline(
                    new KeyFrame(Duration.seconds(finalI / 20.0), event ->
                    {
                        LoadingBar.setProgress(finalI / 100.0);
                        LoadingLabel.setText(finalI + "%");
                        if (this.nbSteps == 0 && finalI == 100 ){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 1;
                            String affichageText = "Bonjour Sergey, ravi de vous revoir, étant donné que votre dérnière connexion remonte à 6 mois, \n je vais procéder à une vérification \n";
                            Text.setText(affichageText + "Pouvez vous me donner le nom de votre chien?" );
                            Instructions.setText("Pouvez vous me donner le nom de votre chien?");
                            LoadingBar.setProgress(0);
                            LoadingLabel.setText("0%");
                            ValidButton.setDisable(false);
                        }
                        else if (this.nbSteps == 1 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 2;
                            String affichageText = "Merci \n";
                            Text.setText(affichageText);
                            Text.setText(affichageText + "Maintenant, à quelle date avez-vous voyagé de paris à lima");
                            Instructions.setText("A quelle date avez-vous voyagé de paris à lima");
                            LoadingBar.setProgress(0);
                            LoadingLabel.setText("0%");
                            ValidButton.setDisable(false);
                        }
                        else if (this.nbSteps == 2 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 3;
                            String affichageText = "Merci \n";
                            Text.setText(affichageText);
                            Text.setText(affichageText + "Pouvez vous me dire le décallage horraire entre ces 2 villes?");
                            Instructions.setText("Pouvez vous me dire le décallage horraire entre ces 2 villes?");
                        }
                        else if (this.nbSteps == 3 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 4;
                            String affichageText = "Merci \n";
                            Text.setText(affichageText);
                            Text.setText(affichageText + "Pouvez vous me dire le décallage horraire entre ces 2 villes?");
                            Instructions.setText("Pouvez vous me dire le décallage horraire entre ces 2 villes?");
                        }
                        else if (this.nbSteps == 4 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 5;
                            String affichageText = "Parfait \n";
                            Text.setText(affichageText);
                            Text.setText(affichageText + "Nous allons passer à la deuxième étape d'identification....?");
                            Instructions.setText("Pouvez vous me donner le n° atomique du darmstadtium?");
                        }
                        else if (this.nbSteps == 5 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 6;
                            String affichageText = "Parfait Sergey, tout m'indique ue c'est bien vous \n";
                            Text.setText(affichageText);
                            Text.setText(affichageText + "Je m'occupe du déverouillage de sécurité \nAfin de déverouiller le SAS j'ai besoin de connaitre la quantité de réactif de Woolins \nprésent dans votre labo");
                            Instructions.setText("Donnez moi la quantité de réactif de Woolins présent dans votre labo");
                        }
                        else if (this.nbSteps == 6 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 7;
                            Text.setText("Maintenant, donnez moi la quantité de bleu de trypon");
                            Instructions.setText("Donnez moi la quantité de bleu de trypon");
                        }
                        else if (this.nbSteps == 7 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 8;
                            Text.setText("Maintenant, donnez moi la quantité de Varadate d'ammonium");
                            Instructions.setText("Donnez moi la quantité de Varadate d'ammonium");
                        }
                        else if (this.nbSteps == 8 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 9;
                            Text.setText("Maintenant, donnez moi la quantité de Jaune de méthyle");
                            Instructions.setText("Donnez moi la quantité de Jaune de méthyle");
                        }
                        else if (this.nbSteps == 9 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 10;
                            Text.setText("Parfait Sergey, voilà le code du premier SAS de décompression : 7989");
                            Instructions.setText("Donnez moi le n° de série du contenant hermétique du virus 'chimère'?");
                        }
                        else if (this.nbSteps == 10 && finalI == 100){
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.nbSteps = 10;
                            Text.setText("Mes analyses me porte à croire que vous n'êtes pas Sergey, j'ai donc procédé à la mise en place \ndu mode 'Bunker'. \nVous êtes actuellement enfermé dans cette salle, veuillez saisir le code de sortie sur le digicode. \n 1-9-8-3");
                            Instructions.setText("Donnez moi le n° de série du contenant hermétique du virus 'chimère'?");
                        }

                    })
            );
            timer.setCycleCount(1);
            timer.play();

        }

    }
}